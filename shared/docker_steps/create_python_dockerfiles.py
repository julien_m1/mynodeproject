#!/usr/bin/env python3

import sys
import json

def create_dockerfile():
    content = """
    FROM python:latest
    WORKDIR /app

    COPY ../requirements.txt .
    RUN pip install --no-cache-dir -r ./requirements.txt

    COPY ../ .
    COPY ../store .
    CMD ["python3", "manage.py", "runserver", "0.0.0.0:8000"]
    ENV PYTHONDONTWRITEBYTECODE=1
    ENV PYTHONUNBUFFERED=1
    """
    return content 

# Rest of the script remains the same


def write_dockerfile():
    dockerfile_content = create_dockerfile()
    with open('Dockerfile', 'w') as file:
        file.write(dockerfile_content)
    print("Dockerfile créé avec succès")
    print("Contenu du Dockerfile créé:")
    with open('Dockerfile', 'r') as f:
        print(f.read())

if __name__ == "__main__":
    write_dockerfile()
