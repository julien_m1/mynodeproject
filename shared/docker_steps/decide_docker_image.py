#!/usr/bin/env python3

import os

REQUIREMENTS_PATH = './requirements.txt'

SLIM_DEPENDENCIES = ['pandas', 'numpy', 'scipy', 'opencv-python'] #à rajouter au fur et à mesure pc j'ai pas trouvé comment faire autrement pour le moment

#déterminer l'image docker à utiliser entre alpine et slim 
def determine_docker_image(requirements_path):
    image_variant = 'alpine' #on présume qu'on utilise Alpine d'abord

    try:
        with open(requirements_path, 'r') as file:
            for line in file:
                #package==1.0.0
                #package>=1.0.0
                #package<2.0,>=1.5
                package = line.strip().split('==')[0].split('>=')[0].split('<')[0].strip() #trouver le nom du package
                if package in SLIM_DEPENDENCIES:
                    image_variant = 'slim'
                    break
    except FileNotFoundError:
        print(f"Le fichier {requirements_path} est introuvable. Utilisation de 'slim' par défaut.")
        for item in os.listdir('.'):
            print(item)
        image_variant = 'slim' #on utilise l'image slim par défaut si le fichier requirement.txt est introuvable

    return image_variant


#placer le résulttat dans le fichier IMAGE_VARIANT pour l'utiliser comme artifact dans GitLab CI
def write_image_variant(variant):
    with open('IMAGE_VARIANT', 'w') as file:
        file.write(variant)

if __name__ == '__main__':
    variant = determine_docker_image(REQUIREMENTS_PATH)
    write_image_variant(variant)
    print(f"Image Docker utilisée : {variant}")
