#!/usr/bin/env python3

import sys
import json

def create_dockerfile():
    content = """FROM php:latest
WORKDIR /var/www/html

COPY ../../ .

RUN docker-php-ext-install pdo pdo_mysql

CMD ["php", "-S", "0.0.0.0:8000", "-t", "/var/www/html"]

"""
    return content

def write_dockerfile():
    dockerfile_content = create_dockerfile()
    with open('Dockerfile', 'w') as file:
        file.write(dockerfile_content)
    print("Dockerfile créé avec succès")
    print("Contenu du Dockerfile créé:")
    with open('Dockerfile', 'r') as f:
        print(f.read())

if __name__ == "__main__":
    write_dockerfile()
