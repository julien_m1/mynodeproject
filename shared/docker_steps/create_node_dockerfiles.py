#!/usr/bin/env python3

import sys
import json

def create_dockerfile():
    content = """FROM node:latest
WORKDIR /app

COPY ../../package.json .
COPY ../../package-lock.json .

RUN npm install
RUN npm install -g prisma
RUN npx prisma generate
RUN npm install -g nodemon

COPY ../../ .

CMD ["nodemon", "index.js"]

"""
    return content

def write_dockerfile():
    dockerfile_content = create_dockerfile()
    with open('Dockerfile', 'w') as file:
        file.write(dockerfile_content)
    print("Dockerfile créé avec succès")
    print("Contenu du Dockerfile créé:")
    with open('Dockerfile', 'r') as f:
        print(f.read())

if __name__ == "__main__":
    write_dockerfile()
