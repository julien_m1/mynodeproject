import gitlab
import requests
import os

# Configuration GitLab
GITLAB_URL = 'https://gitlab.com'
PRIVATE_TOKEN = 'glpat-tJ8pGoKbsswiJZ1FX7Md'
PROJECT_ID = '58140416'
BRANCH = 'main'
JOBS = ['build_script', 'run_pylint', 'snyk_scan', 'build_image']

# Configuration Filebeat
FILEBEAT_URL = 'http://filebeat:5044'

# Connexion à GitLab
gl = gitlab.Gitlab(GITLAB_URL, private_token=PRIVATE_TOKEN)

# Obtenir le projet
project = gl.projects.get(PROJECT_ID)

# Télécharger les artefacts pour chaque job
for job_name in JOBS:
    pipelines = project.pipelines.list(ref=BRANCH, order_by='id', sort='desc', per_page=1)
    if not pipelines:
        print(f"Aucun pipeline trouvé pour la branche {BRANCH}")
        continue

    pipeline = pipelines[0]
    jobs = pipeline.jobs.list()

    for job in jobs:
        if job.name == job_name:
            print(f"Téléchargement des artefacts pour le job {job_name}...")

            # Télécharger les artefacts
            artifact_url = f"{GITLAB_URL}/api/v4/projects/{PROJECT_ID}/jobs/{job.id}/artifacts"
            headers = {'PRIVATE-TOKEN': PRIVATE_TOKEN}
            response = requests.get(artifact_url, headers=headers)
            if response.status_code == 200:
                artifact_path = f"/tmp/{job_name}_artifacts.zip"
                with open(artifact_path, 'wb') as f:
                    f.write(response.content)

                # Extraire les artefacts
                os.system(f"unzip -o {artifact_path} -d /tmp/{job_name}")

                # Lire et envoyer les logs à Filebeat
                log_files = [os.path.join(f"/tmp/{job_name}", file) for file in os.listdir(f"/tmp/{job_name}") if file.endswith('.log')]
                for log_file in log_files:
                    with open(log_file, 'r') as f:
                        for line in f:
                            data = {"message": line.strip()}
                            requests.post(FILEBEAT_URL, json=data)

                print(f"Artefacts du job {job_name} téléchargés et envoyés à Filebeat.")
            else:
                print(f"Erreur lors du téléchargement des artefacts pour le job {job_name}: {response.text}")
