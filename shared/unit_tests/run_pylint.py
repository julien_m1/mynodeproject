#!/usr/bin/env python3
import os
import subprocess

def run_pylint(directory):
    for root, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.py'):
                subprocess.run(['pylint', os.path.join(root, file)])

if __name__ == "__main__":
    run_pylint('/test_project/')
