variable "ARM_CLIENT_ID" {
  description = "The Client ID for Azure."
  type        = string
}

variable "ARM_CLIENT_SECRET" {
  description = "The Client Secret for Azure."
  type        = string
}

variable "ARM_SUBSCRIPTION_ID" {
  description = "The Subscription ID for Azure."
  type        = string
}

variable "ARM_TENANT_ID" {
  description = "The Tenant ID for Azure."
  type        = string
}
