terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 2.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~> 3.0"
    }
  }
}

provider "azurerm" {
  features {}
  client_id       = var.ARM_CLIENT_ID
  client_secret   = var.ARM_CLIENT_SECRET
  subscription_id = var.ARM_SUBSCRIPTION_ID
  tenant_id       = var.ARM_TENANT_ID
}

resource "azurerm_resource_group" "aci_rg" {
  name     = "myNodeResourceGroup"
  location = "East US"
}

resource "random_pet" "name" {
  length    = 2
  separator = "-"
}

resource "azurerm_container_group" "example_aci" {
  name                = "mynodecontainergroup"
  location            = azurerm_resource_group.aci_rg.location
  resource_group_name = azurerm_resource_group.aci_rg.name
  os_type             = "Linux"
  ip_address_type     = "Public"
  dns_name_label      = "myapp-${random_pet.name.id}"

  container {
    name   = "mynodecontainer"
    image  = "mariejuju/node:latest"
    cpu    = "0.5"
    memory = "1.5" 

    ports {
      port     = 3000
      protocol = "TCP"
    }
  }
}
